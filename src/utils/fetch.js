function browserFetch (url) {

  return fetch(url,{mode: 'no-cors'});


  // return window.fetch(url, Object.assign({
  //   method: 'GET',
  //   headers: Object.assign({
  //     'Content-Type': 'application/json; charset=UTF-8'
  //   })
  // }));
}

export default async function (url) {
  if (typeof window !== 'undefined') {
    return browserFetch(url);
  }
}
