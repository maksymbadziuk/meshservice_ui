import  React, { Component } from 'react';
import './App.css';
import List from './List';
import DataTable from 'react-data-table-component';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: '',
      items: []
    };

    this.onFetch();
  }

  prepareColumns(data) {
      const columns = [
          {
              name: 'Id',
              selector: 'id',
              sortable: true,
          },
          {
              name: 'Name',
              selector: 'name',
              sortable: true,
              right: true,
          },
      ];

      if ('description' in data[0]) {
          columns.push( {
              name: 'Description',
              selector: 'description',
              sortable: true,
              right: true,
          })
      }
      columns.push(
          {
              name: 'Rating',
              selector: 'rating',
              sortable: true,
              right: true,
          },
          {
              name: 'Presentation',
              selector: 'presentation',
              right: true,
          });

      return columns;
  }

  onFetch = async (event) => {

      if (event) {
          event.preventDefault();
      }

      let protocol = 'http://';
      let domainPort = process.env.GATEWAY_DOMAIN_PORT;
      let path = '/gateway/books';
      let fetchUrl = protocol + domainPort + path;
      // await fetch(`http://localhost:8080/books`)
      // await fetch(`http://books/gateway/books`)
      await fetch(fetchUrl)
          .then(response =>  response.json())
          .then((myJson) => {

          console.log(myJson);
            this.setState({
              term: '',
              items: myJson,
              columns: this.prepareColumns(myJson)
            });
        });
  }

  render() {
    return (
        <div>
          <form className="App" onSubmit={this.onFetch}>
            <button>Fetch</button>
          </form>

          <DataTable
              title="Books"
              columns={this.state.columns}
              data={this.state.items}
              striped={true}
          />
        </div>
    );
  }

}
