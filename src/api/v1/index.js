import fetch from '../../utils/fetch';

export default {

  async loadBooksResources (data) {
    return Promise.resolve([
       fetch(`http://localhost:8080/books`, {mode: 'no-cors'})
      // this.loadBooks(data)
    ]);
  },

  loadBooks (data) {
    return fetch(`http://localhost:8080/books`);
  },
};
